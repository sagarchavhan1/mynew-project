variable "name" {
  type = string
}
variable "vpc_id" {
  type = string
}
# variable "ingress" {
#   type = string
# }

variable "whitelist" {
  type = list(string)
}
variable "port" {
  type = list(string)
}
