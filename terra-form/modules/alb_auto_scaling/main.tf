# module calling
module "auto_scaling" {
  source              = "../auto_scaling/"
  name                = var.name
  image_id            = var.image_id
  instance_type       = var.instance_type
  key_name            = var.key_name
  security_groups     = ["sg-0ed2d2b7c63088446"]
  max_size            = "3"
  min_size            = "1"
  desired_capacity    = "2"
  vpc_zone_identifier = var.subnets
  target_group_arns   = [module.alb.tg]
}

module "alb" {
  source          = "../alb/"
  name            = var.name
  vpc_id          = var.vpc_id
  security_groups = ["sg-0ed2d2b7c63088446"]
  subnets         = var.subnets
}