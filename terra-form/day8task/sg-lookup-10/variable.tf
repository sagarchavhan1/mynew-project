variable "name" {
  type = list(string)
}
variable "vpc_id" {
  type = string
}
variable "ingress" {
  type = any
}

variable "whitelist" {
  type = list(string)
}
variable "port" {
  type = string
}
