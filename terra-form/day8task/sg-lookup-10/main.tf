provider "aws" {
  region  = "us-east-2"
  profile = "default"
}
resource "aws_security_group" "this" {
  count  = length(var.name)
  name   = var.name[count.index]
  vpc_id = var.vpc_id

  dynamic "ingress" {
    for_each = var.ingress
    content {

      from_port   = ingress.value["port"]
      to_port     = ingress.value["port"]
      protocol    = ingress.value["protocol"]
      cidr_blocks = var.whitelist
    }
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]

  }
}


