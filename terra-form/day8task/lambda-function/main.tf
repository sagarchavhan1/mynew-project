provider "aws" {
  region  = "us-east-2"
  profile = "default"
}
resource "aws_lambda_function" "test_lambda" {
  #filename      = "lambda_function_payload.zip"
  function_name = "lambda_function_name"
  role          = aws_iam_role.iam_for_lambda.arn
  handler       = "index.test"
}