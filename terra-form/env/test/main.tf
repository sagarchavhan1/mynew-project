module "mariadb_rds" {
  source = "../../day8task/mariadb/"

}
module "s3_bucket" {
  source         = "../../day8task/10-user-bucket/"
  name           = ["iam-user", "admin", "unknown", "shubham", "vatan", "demo", "own", "mayur", "kalpak", "akshay", ]
  subnet_id      = ["subnet-32e47559", "subnet-04be8948", "subnet-b8e02fc5"]
  s3_bucket_name = ["prod-bucket-9", "stage-bucket-9", "qa-bucket-9", "prod-bucket-7", "stage-bucket-7", "qa-bucket-7", "prod-bucket-5", "stage-bucket-5", "qa-bucket-5", "prod-bucket-3"]
}


module "route53" {
  source = "../../day8task/route53/"
  name   = ["sub", "sub1", "sub2", "sub3", "sub4"]
  vpc_id = "vpc-0a6e7577e46f37e75"
}