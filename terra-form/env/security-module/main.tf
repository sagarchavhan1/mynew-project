module "security-group" {
  source    = "../../day8task/sg-lookup-10/"
  name      = ["test", "2", "3", "5", "6", "8", "9", "10"]
  vpc_id    = "vpc-0a6e7577e46f37e75"
  port      = ""
  whitelist = ["0.0.0.0/0"]
  ingress = {
    "ssh" = {
      "port"   = 22
      protocol = "tcp"

    }
  }
}