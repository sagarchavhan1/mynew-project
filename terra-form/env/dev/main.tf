provider "aws" {
  region  = "us-east-2"
  profile = "default"
}

module "alb_autoscaling" {
  source              = "../../modules/alb_auto_scaling/"
  name                = "test-app"
  image_id            = "ami-0dd0ccab7e2801812"
  instance_type       = "t2.micro"
  key_name            = "ohio-region"
  security_groups     = ["sg-0ed2d2b7c63088446"]
  max_size            = "3"
  min_size            = "1"
  desired_capacity    = "2"
  vpc_zone_identifier = ["subnet-32e47559", "subnet-04be8948"]
  target_group_arns   = [module.alb.tg]
}

